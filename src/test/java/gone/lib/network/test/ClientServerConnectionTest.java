package gone.lib.network.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.lib.network.contracts.ITcpServerAccepted;
import gone.lib.network.tcp.TcpClientModule;
import gone.lib.network.tcp.TcpServerAccept;

import org.junit.Test;

/**
 * Created by Winfried on 23.09.2016.
 */
public class ClientServerConnectionTest {
    class AcceptedConnection implements ITcpServerAccepted {
        AcceptedConnection() {
            hasAccepted = false;
        }

        @Override
        public void acceptedConnection(String sessionId) {
            hasAccepted = true;
        }

        public boolean hasAccepted;
    }

    @Test
    public void testSimpleConnection() throws InterruptedException {
        AcceptedConnection acceptedConnection = new AcceptedConnection();
        TcpServerAccept accept = new TcpServerAccept(5555, acceptedConnection);
        new Thread(accept).start(); // start listening
        Thread.sleep(500);

        assertFalse(acceptedConnection.hasAccepted);

        new TcpClientModule().connect("localhost", 5555); // do connect
        Thread.sleep(500);

        accept.stop();

        assertTrue(acceptedConnection.hasAccepted);
    }
}