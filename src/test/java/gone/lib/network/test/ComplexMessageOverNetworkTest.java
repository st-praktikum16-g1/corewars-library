package gone.lib.network.test;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Awaitility.fieldIn;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import gone.lib.common.ClientRole;
import gone.lib.common.RcStandard;
import gone.lib.network.CwClient;
import gone.lib.network.CwServer;
import gone.lib.network.contracts.IClient;
import gone.lib.network.contracts.IClientCallback;
import gone.lib.network.contracts.IServer;
import gone.lib.network.contracts.IServerCallback;
import gone.lib.network.json.*;
import gone.lib.network.serialization.IncomingMessagesInterpreter;
import gone.lib.network.serialization.OutgoingMessagesSerializer;
import gone.lib.network.tcp.TcpClientModule;
import gone.lib.network.tcp.TcpServerModule;

import org.junit.Test;

import java.util.LinkedList;
import java.util.concurrent.Callable;
import javax.naming.OperationNotSupportedException;

/**
 * Created by winfr on 30.09.2016.
 */
public class ComplexMessageOverNetworkTest {
    class ServerCallback implements IServerCallback {
        private String sessionId;
        private IServer server;

        public void setServer(IServer server) {
            this.server = server;
        }

        @Override
        public void receiveLoginTelegram(CwLoginContent telegram, String sessionId) {
            this.sessionId = sessionId;
            this.receivedLogin = true;

            CwLoginOkContent loginOkContent = new CwLoginOkContent();

            loginOkContent.lineLength = 100;
            loginOkContent.coreSize = 500;
            loginOkContent.clientMustWait = false;
            loginOkContent.standard = RcStandard.ICWS94;

            this.server.sendLoginOkTelegram(loginOkContent, this.sessionId);
        }

        @Override
        public void receiveReadyTelegram(CwReadyContent telegram, String sessionId) {
            assertThat(telegram.warriorRedcode.size(), is(3));
            assertThat(telegram.warriorRedcode.get(0), is("MOV #4 4"));
            assertThat(telegram.warriorRedcode.get(1), is("ADD 1 1"));
            assertThat(telegram.warriorRedcode.get(2), is("SUB 0 0"));

            receivedReady = true;
        }

        public boolean receivedLogin;
        public boolean receivedReady;
    }

    class ClientCallback implements IClientCallback {
        private IClient client;

        public void setClient(IClient client) {
            this.client = client;
        }

        @Override
        public void receiveLoginOkTelegram(CwLoginOkContent telegram) {
            assertThat(telegram.lineLength, is(100));
            assertThat(telegram.coreSize, is(500));
            assertThat(telegram.clientMustWait, is(false));
            assertThat(telegram.standard, is(RcStandard.ICWS94));

            receivedLoginOk = true;

            CwReadyContent content = new CwReadyContent();

            content.warriorRedcode = new LinkedList<>();
            content.warriorRedcode.add("MOV #4 4");
            content.warriorRedcode.add("ADD 1 1");
            content.warriorRedcode.add("SUB 0 0");

            client.sendReadyTelegram(content);
        }

        @Override
        public void receiveStartTelegram(CwStartContent telegram) {

        }

        @Override
        public void receiveGameStatusTelegram(CwGameStatusContent telegram) {

        }

        @Override
        public void receiveGameResultTelegram(CwGameResultContent telegram) {

        }

        public boolean receivedLoginOk;
    }

    @Test
    public void testComplexScenario() throws InterruptedException, OperationNotSupportedException {
        ComplexMessageOverNetworkTest.ServerCallback serverCallback
                = new ComplexMessageOverNetworkTest.ServerCallback();
        ComplexMessageOverNetworkTest.ClientCallback clientCallback
                = new ComplexMessageOverNetworkTest.ClientCallback();
        CwServer server = new CwServer(serverCallback);
        CwClient client = new CwClient(clientCallback);

        serverCallback.setServer(server);
        clientCallback.setClient(client);

        server.start(5557);
        client.connect("localhost", 5557);

        CwLoginContent login = new CwLoginContent();
        login.role = ClientRole.Player;
        login.name = "name";
        client.sendLoginTelegram(login);

        Callable<Boolean> hasReceivedLogin = fieldIn(serverCallback)
                .ofType(boolean.class).andWithName("receivedReady");
        await().atMost(1, SECONDS).until(hasReceivedLogin);

        assertThat(serverCallback.receivedLogin, is(true));
        assertThat(clientCallback.receivedLoginOk, is(true));
    }
}
