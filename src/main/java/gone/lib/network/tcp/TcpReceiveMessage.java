package gone.lib.network.tcp;

import gone.lib.common.NetworkConstants;
import gone.lib.network.contracts.ITcpListener;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Winfried on 23.09.2016.
 */
public class TcpReceiveMessage implements Runnable {
    private DataInputStream input;
    private boolean waitOnce;
    private String sessionId;
    private List<ITcpListener> listeners = new LinkedList<>();

    public TcpReceiveMessage(DataInputStream input, String sessionId, boolean waitOnce) {
        this.input = input;
        this.sessionId = sessionId;
        this.waitOnce = waitOnce;
    }

    public void addListener(ITcpListener listener) {
        listeners.add(listener);
    }

    /**
     * Asynchronously receives incoming messages
     */
    @Override
    public void run() {
        try {
            while (!waitOnce) {
                String data = this.input.readUTF();

                for (ITcpListener listener : listeners) {
                    listener.receiveMessage(this.sessionId, data);
                }
            }
        } catch (Exception exception) {
            //exception.printStackTrace();
        }
    }
}
