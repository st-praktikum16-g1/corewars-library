package gone.lib.network.tcp;

import gone.lib.network.contracts.ITcpServerAccepted;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Winfried on 23.09.2016.
 */
public class TcpServerAccept implements Runnable {
    private ITcpServerAccepted accepted;
    private int serverPort;
    private SecureRandom random = new SecureRandom();
    private ServerSocket welcomeSocket;

    public Map<String, TcpClientConnection> clientConnections = new HashMap<>();

    public TcpServerAccept(int serverPort, ITcpServerAccepted accepted) {
        this.serverPort = serverPort;
        this.accepted = accepted;
    }

    /**
     * Asynchronously wait for incoming clients
     */
    @Override
    public void run() {
        try {
            welcomeSocket = new ServerSocket(this.serverPort);

            // accept incoming clients
            while (true) {
                Socket clientSocket = welcomeSocket.accept();
                DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());

                TcpClientConnection newConnection = new TcpClientConnection();
                newConnection.clientSocket = clientSocket;
                newConnection.output = output;
                newConnection.input = input;

                String sessionId = nextSessionId();
                clientConnections.put(sessionId, newConnection);
                this.accepted.acceptedConnection(sessionId);
            }
        } catch (Exception exception) {
            //exception.printStackTrace();
        }
    }

    public void stop() {
        for (TcpClientConnection clientConnection : this.clientConnections.values()) {
            try {
                clientConnection.clientSocket.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        try {
            welcomeSocket.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Generates a unique session id.
     * @return the session id
     */
    private String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }
}