package gone.lib.network.serialization;

import gone.lib.common.NetworkConstants;

import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwReadyContent;
import gone.lib.network.json.CwStartContent;
import org.json.JSONStringer;

/**
 * Created by winfr on 26.08.2016.
 */
public class OutgoingMessagesSerializer {
    private int packetVersion;

    public OutgoingMessagesSerializer() {
        // TODO: Init this from config
        packetVersion = 1;
    }

    public String serializeGameStatusMessage(CwGameStatusContent content) {
        String result = new JSONStringer()
                .object()
                    .key("content")
                    .object()
                        .key("indexOfChangedCoreElement").value(content.indexOfChangedCoreElement)
                        .key("newOpCode").value(content.newOpCode)
                        .key("oldIpIndex").value(content.oldIpIndex)
                        .key("newIpIndex").value(content.newIpIndex)
                        .key("playerOneActive").value(content.playerOneActive)
                    .endObject()
                    .key("version").value(packetVersion)
                    .key("packageType").value("status")
                .endObject().toString();
        return result;
    }

    public String serializeLoginMessage(CwLoginContent content) {
        String result = new JSONStringer()
                .object()
                    .key("content")
                    .object()
                        .key("name").value(content.name)
                        .key("role").value(content.role.toString())
                    .endObject()
                    .key("version").value(packetVersion)
                    .key("packageType").value("login")
                .endObject().toString();
        return result;
    }

    public String serializeReadyMessage(CwReadyContent content) {
        String result = new JSONStringer()
                .object()
                    .key("content")
                    .object()
                        .key("warriorRedcode").value(String.join(
                                NetworkConstants.REDCODESEPARATOR, content.warriorRedcode))
                    .endObject()
                    .key("version").value(packetVersion)
                    .key("packageType").value("ready")
                .endObject().toString();
        return result;
    }

    public String serializeLoginOkMessage(CwLoginOkContent content) {
        String result = new JSONStringer()
                .object()
                    .key("content")
                    .object()
                        .key("clientMustWait").value(content.clientMustWait)
                        .key("coreSize").value(content.coreSize)
                        .key("lineLength").value(content.lineLength)
                        .key("standard").value(content.standard.toString())
                    .endObject()
                    .key("version").value(packetVersion)
                    .key("packageType").value("loginOk")
                .endObject().toString();
        return result;
    }

    public String serializeStartMessage(CwStartContent content) {
        String result = new JSONStringer()
                .object()
                    .key("content")
                    .object()
                        .key("fullState").value(String.join(
                                NetworkConstants.REDCODESEPARATOR, content.fullState))
                        .key("players").value(content.players)
                    .endObject()
                    .key("version").value(packetVersion)
                    .key("packageType").value("start")
                .endObject().toString();
        return result;
    }

    public String serializeGameResultMessage(CwGameResultContent content) {
        String result = new JSONStringer()
                .object()
                .key("content")
                .object()
                    .key("state").value(content.state)
                .endObject()
                .key("version").value(packetVersion)
                .key("packageType").value("finish")
                .endObject().toString();
        return result;
    }
}