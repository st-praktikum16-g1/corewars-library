package gone.lib.network.json;

import java.util.List;

/**
 * message type to send the changes of the
 * server core initiation and the count of the competing players
 * from the server to the client
 *
 * @author winfried
 */
public class CwStartContent {
    public int players;
    public List<String> fullState;
}
