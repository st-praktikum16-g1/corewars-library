package gone.lib.network.json;

/**
 * message type the clients receive if the game ends
 *
 * @author winfried
 */
public class CwGameResultContent {
    public String state;
}