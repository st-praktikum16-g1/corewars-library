package gone.lib.network.json;

import gone.lib.common.ClientRole;

/**
 * message type that will be sent from the client to
 * the server to establish a login on the server
 *
 * @author winfried
 */
public class CwLoginContent {
    public String name;
    public ClientRole role;
}
