package gone.lib.network.json;

import gone.lib.common.RcStandard;

/**
 * message type that receives the client from the server
 * if the previous login of the client was successful
 *
 * @author winfried
 */
public class CwLoginOkContent {
    public boolean clientMustWait;
    public RcStandard standard;
    public int coreSize;
    public int lineLength;
}
