package gone.lib.network.json;

import java.util.List;

/**
 * message type to send the warrior code from the client
 * to the server
 *
 * @author Winfried
 */
public class CwReadyContent {
    public List<String> warriorRedcode;
}
