package gone.lib.network.contracts;

import gone.lib.network.json.CwLoginContent;
import gone.lib.network.json.CwReadyContent;

import javax.naming.OperationNotSupportedException;

/**
 * Created by winfr on 27.09.2016.
 */
public interface IClient {
    void connect(String address, int port) throws OperationNotSupportedException;

    void disconnect();

    void sendLoginTelegram(CwLoginContent telegram);

    void sendReadyTelegram(CwReadyContent telegram);

    IClientCallback getCallback();

    boolean getIsConnected();
}
