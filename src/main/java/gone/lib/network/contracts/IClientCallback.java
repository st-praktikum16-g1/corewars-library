package gone.lib.network.contracts;

import gone.lib.network.json.CwGameResultContent;
import gone.lib.network.json.CwGameStatusContent;
import gone.lib.network.json.CwLoginOkContent;
import gone.lib.network.json.CwStartContent;

/**
 * Created by winfr on 27.09.2016.
 */
public interface IClientCallback {
    void receiveLoginOkTelegram(CwLoginOkContent telegram);

    void receiveStartTelegram(CwStartContent telegram);

    void receiveGameStatusTelegram(CwGameStatusContent telegram);

    void receiveGameResultTelegram(CwGameResultContent telegram);
}
