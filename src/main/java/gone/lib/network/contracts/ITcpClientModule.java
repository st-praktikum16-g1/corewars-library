package gone.lib.network.contracts;

/**
 * Created by Winfried on 23.09.2016.
 */
public interface ITcpClientModule {
    void connect(String address, int port);

    void sendMessage(String data);

    void disconnect();

    void registerListener(ITcpListener listener);

    void removeListener(ITcpListener listener);
}
