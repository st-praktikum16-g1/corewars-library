package gone.lib.network.contracts;

import gone.lib.network.tcp.TcpClientConnection;

/**
 * Created by winfr on 26.09.2016.
 */
public interface ITcpServerAccepted {
    void acceptedConnection(String sessionId);
}
