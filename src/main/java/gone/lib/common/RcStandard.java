package gone.lib.common;

/**
 * store the different possible rc standards
 *
 * @author  matthias
 */
public enum RcStandard {
    ICWS88,
    ICWS94
}
