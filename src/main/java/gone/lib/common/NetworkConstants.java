package gone.lib.common;

/**
 * constant strings we regularly use for the network communication
 *
 * @author Winfried
 */
public final class NetworkConstants {
    public static final String STARTINGLINE = "--..--";
    public static final String FINISHINGLINE = "..--..";
    public static final String REDCODESEPARATOR = "§";
    public static final String FULLSTATESEPARATOR = "§";
}
