package gone.lib.common;

/**
 * all compatible languages for the client/ui
 *
 * @author matthias
 */
public enum Language {
    de,
    en
}
