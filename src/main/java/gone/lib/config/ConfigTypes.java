package gone.lib.config;

/**
 * possible configuration types
 *
 * @author matthias
 */
public enum ConfigTypes {
    CLIENT_CONFIG,
    SERVER_CONFIG
}
