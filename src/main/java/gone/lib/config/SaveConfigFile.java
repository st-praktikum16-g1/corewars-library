package gone.lib.config;

import gone.lib.common.Language;
import gone.lib.common.RcStandard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * updates the user config file with new values
 * the config file is stored as a preferences file (xml key=value, utf8)
 * xml dtd used: http://java.sun.com/dtd/preferences.dtd
 *
 *  @author matthias
 */
public class SaveConfigFile {
    /**
     * simply using the private save method, catch exceptions
     * @param actualParameters
     * @param userFilePath
     * @throws IOException
     */
    public SaveConfigFile(ConfigParameters actualParameters, String userFilePath,
                          ConfigTypes configType) throws IOException {
        if (configType.equals(ConfigTypes.CLIENT_CONFIG)) {
            saveClientConfig((ClientConfig) actualParameters, userFilePath, configType.toString());
        } else {
            saveServerConfig((ServerConfig) actualParameters, userFilePath, configType.toString());
        }
    }

    /**
     * transfer the config parameters to a preferences object and save this
     * to the user config file
     * @param params our client config object that stores the actual parameters
     * @param userFilePath location to save the config file
     * @param nodeName string representing the config type
     * @throws IOException if file is not found
     */
    private void saveClientConfig(ClientConfig params, String userFilePath, String nodeName)
            throws IOException {

        if (userFilePath.isEmpty()) {
            throw new IllegalArgumentException("user file path should not be empty!");
        }

        File input = new File(userFilePath);
        if (!input.exists()) {
            input.getParentFile().mkdirs();
        }

        Preferences userPrefs = Preferences.userRoot().node(nodeName);

        userPrefs.putInt("WIDTH", params.getResolutionWidth());
        userPrefs.putInt("HEIGHT", params.getResolutionHeight());
        userPrefs.put("WARRIOR_NAME", params.getWarriorName());
        userPrefs.put("PATH_TO_RC_FILE", params.getPathToRcFile());
        userPrefs.putInt("SERVER_PORT", params.getPortLastServer());
        userPrefs.put("SERVER_IP", params.getInetAddress().getHostAddress());
        userPrefs.put("SERVER_NAME", params.getInetAddress().getHostName());

        String rcStd = (params.getRcStandard().toString()).equals(RcStandard.ICWS88.toString())
                ? RcStandard.ICWS88.toString() : RcStandard.ICWS94.toString();
        userPrefs.put("RC_STANDARD", rcStd);

        String lang = (params.getRcStandard().toString()).equals(Language.de.toString())
                ? Language.de.toString() : Language.en.toString();
        userPrefs.put("LANGUAGE", lang);

        FileOutputStream out = new FileOutputStream(userFilePath);
        try {
            userPrefs.exportNode(out);
        } catch (IOException | BackingStoreException exc) {
            exc.printStackTrace();
        }
        out.close();
    }

    /**
     * transfer the config parameters to a preferences object and save this
     * to the user config file
     * @param params our client config object that stores the actual parameters
     * @param userFilePath location to save the config file
     * @param nodeName string representing the config type
     * @throws IOException if file is not found
     */
    private void saveServerConfig(ServerConfig params, String userFilePath, String nodeName)
            throws IOException {

        if (userFilePath.isEmpty()) {
            throw new IllegalArgumentException("user file path should not be empty!");
        }

        File input = new File(userFilePath);
        if (!input.exists()) {
            input.getParentFile().mkdirs();
        }

        Preferences userPrefs = Preferences.userRoot().node(nodeName);
        userPrefs.putInt("MAX_START_INSTRUCTIONS_COUNT", params.getMaxStartInstructions());
        userPrefs.putInt("MAX_PLAYER_COUNT", params.getMaxPlayersCount());
        userPrefs.putInt("MAX_CYCLES", params.getMaxCycles());
        userPrefs.putInt("CORE_SIZE", params.getCoreSize());
        userPrefs.putInt("PORT", params.getPort());
        userPrefs.putInt("WAIT_TIME_MS", params.getWaitTimeMs());

        FileOutputStream out = new FileOutputStream(userFilePath);
        try {
            userPrefs.exportNode(out);
        } catch (IOException | BackingStoreException exc) {
            exc.printStackTrace();
        }
        out.close();
    }
}
