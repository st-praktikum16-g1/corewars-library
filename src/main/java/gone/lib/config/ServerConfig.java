package gone.lib.config;

/**
 * simple data type to store server config variables
 *
 * @author matthias
 */
public class ServerConfig extends ConfigParameters {
    private int maxCycles;
    private int maxStartInstructions;
    private int port;
    private int maxPlayersCount;
    private int coreSize;
    private int waitTimeMs;

    /* constructor */

    public ServerConfig(int maxCycles, int maxStartInstructions, int port, int maxPlayersCount,
                        int coreSize, int waitTimeMs) {
        setMaxCycles(maxCycles);
        setMaxStartInstructions(maxStartInstructions);
        setPort(port);
        setMaxPlayersCount(maxPlayersCount);
        setCoreSize(coreSize);
        setWaitTimeMs(waitTimeMs);
    }

    /* setter */

    public void setWaitTimeMs(int waitTimeMs) {
        if (waitTimeMs <= 0) {
            throw new IllegalArgumentException("wait time milliseconds must be > 0");
        }
        this.waitTimeMs = waitTimeMs;
    }

    public void setMaxCycles(int maxCycles) {
        if (maxCycles <= 0) {
            throw new IllegalArgumentException("max cycles must be > 0");
        }
        this.maxCycles = maxCycles;
    }

    public void setMaxStartInstructions(int maxStartInstructions) {
        if (maxStartInstructions <= 0 || maxStartInstructions > 2000) {
            throw new IllegalArgumentException("maxStartInstructions must be > 0 && <= 2000");
        }
        this.maxStartInstructions = maxStartInstructions;
    }

    public void setPort(int port) {
        // well know ports, tcp range
        if (port < 1024 || port > 65535) {
            throw new IllegalArgumentException("port must be >= 1024 && <= 65535");
        }
        this.port = port;
    }

    public void setMaxPlayersCount(int maxPlayersCount) {
        if (maxPlayersCount != 1 && maxPlayersCount != 2) {
            throw new IllegalArgumentException("maxPlayersCount must be 1 or 2");
        }
        this.maxPlayersCount = maxPlayersCount;
    }

    public void setCoreSize(int coreSize) {
        if (coreSize <= 0) {
            throw new IllegalArgumentException("max cycles must be > 0");
        }
        this.coreSize = coreSize;
    }

    /* getter */

    public int getWaitTimeMs() {
        return waitTimeMs;
    }

    public int getMaxCycles() {
        return maxCycles;
    }

    public int getMaxStartInstructions() {
        return maxStartInstructions;
    }

    public int getPort() {
        return port;
    }

    public int getMaxPlayersCount() {
        return maxPlayersCount;
    }

    public int getCoreSize() {
        return coreSize;
    }
}
